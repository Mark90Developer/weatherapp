#download the image that having jdk8
FROM openjdk:8-jdk-alpine

#create a variable to use in build
ARG JAR_FILE=target/*.jar

#copy the content of jar into new jar file "app.jar"
COPY ${JAR_FILE} app.jar

#direct execution type , call directly execution file (alternatively shell mode run in /bin/sh -c)
ENTRYPOINT ["java","-jar","/app.jar"]