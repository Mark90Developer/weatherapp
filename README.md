# HOW TO RUN PROJECT #

### Prerequisites ###
Install JDK 8 
Install maven 3.8.1

Set JAVA_HOME in environment variable and path to use JDK 8
Set MAVEN_HOME in environmnet variable and path to use maven 

### Checks and compiling ###
Open command line, go in the "weather" project folder  and  perform the following commands to check if the version is rightly installed  and to compile the project:

* java -version			
* mvn -version				
* mvn clean install -U	

after last command should find in weather/target folder 
weather-0.0.1-SNAPSHOT.jar

execute the following command to run the project:
* java -jar weather-0.0.1-SNAPSHOT.jar


### Perform the request ###
Swagger URL:
* http://localhost:8084/swagger-ui.html

Request URL:
* http://localhost:8084/weather/city/Bari

Database h2:
* http://localhost:8084/h2-console/

***
if you want change port of application,you can find the configuration in
/weather/src/main/resources/application.properties







