package com.weather.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.weather.controller.weather.WeatherFourtyEightHoursResponse;
import com.weather.model.weather.Hour;
import com.weather.service.WeatherService;

@SpringBootTest
@AutoConfigureMockMvc
public class WeatherControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private WeatherService weatherService;

	@Test
	public void retrieveWeatherByCityFor48hoursTest() throws Exception {

		WeatherFourtyEightHoursResponse eatherFourtyEightHoursExpectedResponse = new WeatherFourtyEightHoursResponse();
		eatherFourtyEightHoursExpectedResponse.setCityName("bari");
		eatherFourtyEightHoursExpectedResponse.setHours(new ArrayList<Hour>());
		eatherFourtyEightHoursExpectedResponse.getHours().add(new Hour());
		eatherFourtyEightHoursExpectedResponse.getHours().get(0).setDt(1624999105);
		eatherFourtyEightHoursExpectedResponse.getHours().get(0).setFeels_like(311.05);
		eatherFourtyEightHoursExpectedResponse.getHours().get(0).setHumidity(76);
		eatherFourtyEightHoursExpectedResponse.getHours().get(0).setTemp(25);

		ObjectMapper objMap = new ObjectMapper();
		String expectedResponseJson = objMap.writeValueAsString(eatherFourtyEightHoursExpectedResponse);
		when(weatherService.retrieveWeatherByCityFor48hours("bari")).thenReturn(eatherFourtyEightHoursExpectedResponse);

		this.mockMvc.perform(get("/weather/city/{city}", "bari")).andExpect(status().isOk())
				.andExpect(content().json(expectedResponseJson));
	}

	@Test
	public void retrieveWeatherByCityFor48hoursBadRequestTest() throws Exception {

		WeatherFourtyEightHoursResponse eatherFourtyEightHoursExpectedResponse = new WeatherFourtyEightHoursResponse();

		when(weatherService.retrieveWeatherByCityFor48hours("bari")).thenReturn(eatherFourtyEightHoursExpectedResponse);

		this.mockMvc.perform(get("/weather/city/{city}", new Long(123L))).andExpect(status().isBadRequest());

	}

	@Test
	public void retrieveWeatherByCityFor48hoursNull() throws Exception {

		Assertions.assertThrows(IllegalArgumentException.class,
				() -> this.mockMvc.perform(get("/weather/city/{city}", null)));

	}

	@Test
	public void retrieveWeatherByCityFor48hoursOnlyAlphabetAndComma() throws Exception {

		this.mockMvc.perform(get("/weather/city/{city}", "1234")).andExpect(status().isBadRequest());

	}

}
