package com.weather.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.weather.client.WeatherClient;
import com.weather.client.response.common.Coord;
import com.weather.client.response.common.Hourly;
import com.weather.controller.weather.WeatherFourtyEightHoursResponse;

@SpringBootTest
public class WeatherServiceTest {

	@Autowired
	WeatherService weatherService;

	@MockBean
	private WeatherClient weatherClient;

	@Test
	public void weatherretrieveWeatherByCityFor48hoursTestOK() {

		String city = "bari";
		Coord coord = new Coord();
		coord.setLat(11);
		coord.setLon(13);

		List<Hourly> responseWeatherFor24Hours = new ArrayList<Hourly>();
		responseWeatherFor24Hours.add(new Hourly());
		responseWeatherFor24Hours.get(0).setHumidity(1);
		responseWeatherFor24Hours.get(0).setFeels_like(1l);
		responseWeatherFor24Hours.get(0).setTemp(1l);
		responseWeatherFor24Hours.get(0).setDt(123);

		when(weatherClient.getWeatherAndCoordinatesByCity(city)).thenReturn(coord);
		when(weatherClient.getWeatherFor24HoursByCoordinate(coord)).thenReturn(responseWeatherFor24Hours);

		WeatherFourtyEightHoursResponse response = weatherService.retrieveWeatherByCityFor48hours(city);

		assertEquals("bari", response.getCityName());
		assertEquals(123, response.getHours().get(0).getDt());
		assertEquals(1l, response.getHours().get(0).getHumidity());

	}

}

//when(restTemplate.getForEntity(Mockito.eq(configurationApp.getBaseUrl() + queryStringByCity),
//ArgumentMatchers.<Class<WeatherResponse>>any(), Mockito.eq(city), Mockito.eq("123123123123123123")))
//		.thenReturn(rootResponseByCity);
//when(restTemplate.getForEntity(Mockito.eq(configurationApp.getBaseUrl() + queryStringByCoordinates),
//		ArgumentMatchers.<Class<WeatherResponse>>any(), Mockito.anyDouble(), Mockito.anyDouble(),
//		Mockito.eq("123123123123123123"))).thenReturn(rootResponseByCoordinate);