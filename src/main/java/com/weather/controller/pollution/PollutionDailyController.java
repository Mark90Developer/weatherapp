package com.weather.controller.pollution;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.weather.service.PollutionService;

@RestController
@Validated
@RequestMapping("/pollution")

public class PollutionDailyController {

	@Autowired
	private PollutionService pollutionService;

	@RequestMapping(method = RequestMethod.GET, value = "/city/{city}")
	public ResponseEntity<PollutionDailyResponse> pollutionDaily(
			@PathVariable(required = true) @NotEmpty @Pattern(regexp = "[A-Za-z,]+") String city) {
		return ResponseEntity.ok().body(pollutionService.retrieveDailyPollutionByCity(city));

	}

	@RequestMapping(method = RequestMethod.POST, value = "/city")
	public ResponseEntity<PollutionDailyResponse> savePollutionDaily(PollutionDailyRequest pollutionDailyRequest) {

		pollutionService.storeDailyPollutionByCity(pollutionDailyRequest);
		return ResponseEntity.ok().build();

	}

}
