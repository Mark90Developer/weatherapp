package com.weather.controller.pollution;

import com.weather.model.pollution.Components;

import lombok.Data;

@Data
public class PollutionDailyResponse {

	private Integer dt;
	private String CityName;
	private Components components;

}
