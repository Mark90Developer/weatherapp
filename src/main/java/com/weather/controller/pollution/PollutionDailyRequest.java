package com.weather.controller.pollution;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.weather.model.pollution.Components;

import lombok.Data;

@Data
public class PollutionDailyRequest {

	@NotNull
	private Integer dt;
	@NotEmpty
	@NotNull
	private String cityName;
	private Components components;

}
