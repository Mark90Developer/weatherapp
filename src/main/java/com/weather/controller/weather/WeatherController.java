package com.weather.controller.weather;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.weather.service.WeatherService;

@RestController
@Validated
@RequestMapping("/weather")

public class WeatherController {

	@Autowired
	private WeatherService weatherService;

	@RequestMapping(method = RequestMethod.GET, value = "/city/{city}")
	public ResponseEntity<WeatherFourtyEightHoursResponse> weatherFor48Hour(
			@PathVariable(required = true) @NotEmpty @Pattern(regexp = "[A-Za-z,]+") String city) {
		return ResponseEntity.ok().body(weatherService.retrieveWeatherByCityFor48hours(city));

	}

}
