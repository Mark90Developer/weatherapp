package com.weather.controller.weather;

import java.util.List;

import com.weather.model.weather.Hour;

import lombok.Data;

@Data
public class WeatherFourtyEightHoursResponse {

	private String CityName;
	private List<Hour> hours;

}
