package com.weather.converter;

import org.springframework.stereotype.Component;

import com.weather.controller.pollution.PollutionDailyRequest;
import com.weather.dao.entity.PollutionDaily;
import com.weather.model.pollution.Components;

@Component
public class PollutionDailyConverter extends Converter<PollutionDailyRequest, PollutionDaily> {

	public PollutionDailyConverter() {
		super(PollutionDailyConverter::convertToEntity, PollutionDailyConverter::convertToDto);
	}

	private static PollutionDailyRequest convertToDto(PollutionDaily pollutionDaily) {
		PollutionDailyRequest pollutionDailyRequest = new PollutionDailyRequest();
		Components components = new Components();
		pollutionDailyRequest.setCityName(pollutionDaily.getCityName());
		pollutionDailyRequest.setComponents(components);
		pollutionDailyRequest.setDt(pollutionDaily.getDt());
		pollutionDailyRequest.getComponents().setCo(pollutionDaily.getCo());
		pollutionDailyRequest.getComponents().setNh3(pollutionDaily.getNh3());
		pollutionDailyRequest.getComponents().setNo(pollutionDaily.getNo());
		pollutionDailyRequest.getComponents().setNo2(pollutionDaily.getNo2());
		pollutionDailyRequest.getComponents().setO3(pollutionDaily.getO3());
		pollutionDailyRequest.getComponents().setPm10(pollutionDaily.getPm10());
		pollutionDailyRequest.getComponents().setPm25(pollutionDaily.getPm25());
		pollutionDailyRequest.getComponents().setSo2(pollutionDaily.getSo2());

		return pollutionDailyRequest;
	}

	private static PollutionDaily convertToEntity(PollutionDailyRequest pollutionDailyRequest) {
		PollutionDaily pollutionDaily = new PollutionDaily();
		pollutionDaily.setCityName(pollutionDailyRequest.getCityName());
		pollutionDaily.setDt(pollutionDailyRequest.getDt());
		pollutionDaily.setCo(pollutionDailyRequest.getComponents().getCo());
		pollutionDaily.setNh3(pollutionDailyRequest.getComponents().getNh3());
		pollutionDaily.setNo(pollutionDailyRequest.getComponents().getNo());
		pollutionDaily.setNo2(pollutionDailyRequest.getComponents().getNo2());
		pollutionDaily.setO3(pollutionDailyRequest.getComponents().getO3());
		pollutionDaily.setPm10(pollutionDailyRequest.getComponents().getPm10());
		pollutionDaily.setPm25(pollutionDailyRequest.getComponents().getPm25());
		pollutionDaily.setSo2(pollutionDailyRequest.getComponents().getSo2());
		return pollutionDaily;
	}
}
