package com.weather.model.pollution;

import com.weather.model.pollution.Components;

import lombok.Data;

@Data
public class PollutionDailyModel {

	private Integer dt;
	private String CityName;
	private Components components;

}
