package com.weather.model.exception;

public class GeneralException extends Exception {

	private static final long serialVersionUID = 1L;
	private String numberError;

	public GeneralException(String numberError, String message) {
		super(message);
		this.numberError = numberError;
	}

}
