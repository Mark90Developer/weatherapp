package com.weather.model.weather;

import lombok.Data;

@Data
public class Coordinate {
	private double longitude;
	private double latitude;
}