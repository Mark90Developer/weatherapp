package com.weather.model.weather;



import lombok.Data;

@Data
public class Hour{
	private int dt;
	private double temp;
	private double feels_like;
	private int humidity;
}
