package com.weather.exceptionManager;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.weather.exceptionManager.error.FactoryError;
import com.weather.exceptionManager.error.IError;
import com.weather.exceptionManager.model.ErrorBuilder;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice //
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

	// l'applicativo lancia l'exception a runtime e viene catturato nelle api
	// sia in entrata che in uscita
	// si verifica il tipo di exception e si gestisce la risposta

	// 400 ArgumentNotValid
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		/*
		 * List<String> errors = new ArrayList<String>(); for (FieldError error :
		 * ex.getBindingResult().getFieldErrors()) { errors.add(error.getField() + ": "
		 * + error.getDefaultMessage()); } for (ObjectError error :
		 * ex.getBindingResult().getGlobalErrors()) { errors.add(error.getObjectName() +
		 * ": " + error.getDefaultMessage()); }
		 * 
		 * ErrorBuilder apiError = new ErrorBuilder(HttpStatus.BAD_REQUEST,
		 * ex.getLocalizedMessage(), errors); return handleExceptionInternal(ex,
		 * apiError, headers, apiError.getStatus(), request);
		 */
		;
		IError error = FactoryError.createError(FactoryError.invalidParameterError);
		ErrorBuilder errBuild = error.create(ex);
		return handleExceptionInternal(ex, errBuild, headers, errBuild.getStatus(), request);

	}

	/*
	 * 
	 * 
	 * 
	 * @Override protected ResponseEntity<Object>
	 * handleMissingServletRequestParameter(MissingServletRequestParameterException
	 * ex, HttpHeaders headers, HttpStatus status, WebRequest request) { String
	 * error = ex.getParameterName() + " parameter is missing";
	 * 
	 * ErrorBuilder apiError = new ErrorBuilder(HttpStatus.BAD_REQUEST,
	 * ex.getLocalizedMessage(), error); return new ResponseEntity<Object>(apiError,
	 * new HttpHeaders(), apiError.getStatus()); }
	 * 
	 * @Override protected ResponseEntity<Object>
	 * handleMissingPathVariable(MissingPathVariableException ex, HttpHeaders
	 * headers, HttpStatus status, WebRequest request) { String error =
	 * ex.getParameter() + " parameter is missing";
	 * 
	 * ErrorBuilder apiError = new ErrorBuilder(HttpStatus.BAD_REQUEST,
	 * ex.getLocalizedMessage(), error); return new ResponseEntity<Object>(apiError,
	 * new HttpHeaders(), apiError.getStatus());
	 * 
	 * }
	 * 
	 */

	@ExceptionHandler(ConstraintViolationException.class)
	public ResponseEntity<Object> handleConstraintViolation(ConstraintViolationException ex, WebRequest request) {
		List<String> errors = new ArrayList<String>();
		for (ConstraintViolation<?> violation : ex.getConstraintViolations()) {
			errors.add(violation.getRootBeanClass().getName() + " " + violation.getPropertyPath() + ": "
					+ violation.getMessage());
		}

		ErrorBuilder apiError = ErrorBuilder.builder().status(HttpStatus.BAD_REQUEST).message(ex.getLocalizedMessage())
				.errors(null).build();
		return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
	}

	/*
	 * 
	 * @ExceptionHandler({ MethodArgumentTypeMismatchException.class }) public
	 * ResponseEntity<Object>
	 * handleMethodArgumentTypeMismatch(MethodArgumentTypeMismatchException ex,
	 * WebRequest request) { String error = ex.getName() + " should be of type " +
	 * ex.getRequiredType().getName();
	 * 
	 * ErrorBuilder apiError = new ErrorBuilder(HttpStatus.BAD_REQUEST,
	 * ex.getLocalizedMessage(), error); return new ResponseEntity<Object>(apiError,
	 * new HttpHeaders(), apiError.getStatus()); }
	 * 
	 * @Override protected ResponseEntity<Object>
	 * handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders
	 * headers, HttpStatus status, WebRequest request) { String error =
	 * "No handler found for " + ex.getHttpMethod() + " " + ex.getRequestURL();
	 * 
	 * ErrorBuilder apiError = new ErrorBuilder(HttpStatus.NOT_FOUND,
	 * ex.getLocalizedMessage(), error); return new ResponseEntity<Object>(apiError,
	 * new HttpHeaders(), apiError.getStatus()); }
	 * 
	 * @Override protected ResponseEntity<Object>
	 * handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException
	 * ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
	 * StringBuilder builder = new StringBuilder(); builder.append(ex.getMethod());
	 * builder.
	 * append(" method is not supported for this request. Supported methods are ");
	 * ex.getSupportedHttpMethods().forEach(t -> builder.append(t + " "));
	 * 
	 * ErrorBuilder apiError = new ErrorBuilder(HttpStatus.METHOD_NOT_ALLOWED,
	 * ex.getLocalizedMessage(), builder.toString()); return new
	 * ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus()); }
	 * 
	 * // MediaType Unsopported Exception
	 * 
	 * @Override protected ResponseEntity<Object>
	 * handleHttpMediaTypeNotSupported(HttpMediaTypeNotSupportedException ex,
	 * HttpHeaders headers, HttpStatus status, WebRequest request) { StringBuilder
	 * builder = new StringBuilder(); builder.append(ex.getContentType());
	 * builder.append(" media type is not supported. Supported media types are ");
	 * ex.getSupportedMediaTypes().forEach(t -> builder.append(t + ", "));
	 * 
	 * ErrorBuilder apiError = new ErrorBuilder(HttpStatus.UNSUPPORTED_MEDIA_TYPE,
	 * ex.getLocalizedMessage(), builder.substring(0, builder.length() - 2)); return
	 * new ResponseEntity<Object>(apiError, new HttpHeaders(),
	 * apiError.getStatus()); }
	 * 
	 * // catch all exception not previously managed
	 * 
	 * @ExceptionHandler({ Exception.class }) public ResponseEntity<Object>
	 * handleAll(Exception ex, WebRequest request) { ErrorBuilder apiError = new
	 * ErrorBuilder(HttpStatus.INTERNAL_SERVER_ERROR, ex.getLocalizedMessage(),
	 * "error occurred"); return new ResponseEntity<Object>(apiError, new
	 * HttpHeaders(), apiError.getStatus()); }
	 */
}