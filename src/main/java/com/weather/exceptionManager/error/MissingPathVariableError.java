package com.weather.exceptionManager.error;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.MissingPathVariableException;

import com.weather.exceptionManager.model.Detail;
import com.weather.exceptionManager.model.ErrorBuilder;
import com.weather.exceptionManager.model.ErrorCode;

public class MissingPathVariableError implements IError<MissingPathVariableException> {

	@Override
	public ErrorBuilder create(MissingPathVariableException ex) {

		List<Detail> listDetail = new ArrayList<Detail>();

		return ErrorBuilder.builder().status(ErrorCode.INVALID_FIELD.getHttpStatus()).errors(listDetail)
				.message(ex.getLocalizedMessage()).build();

	}

}
