package com.weather.exceptionManager.error;

import com.weather.exceptionManager.model.ErrorBuilder;

public interface IError<T extends Exception> {

	public abstract ErrorBuilder create(T exception);

}
