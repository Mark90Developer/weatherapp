package com.weather.exceptionManager.error;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.MethodArgumentNotValidException;

import com.weather.exceptionManager.model.Detail;
import com.weather.exceptionManager.model.ErrorBuilder;
import com.weather.exceptionManager.model.ErrorCode;

public class InvalidParameterError implements IError<MethodArgumentNotValidException> {

	@Override
	public ErrorBuilder create(MethodArgumentNotValidException ex) {

		List<Detail> listDetail = new ArrayList<Detail>();
		ex.getBindingResult().getFieldErrors().stream().forEach(fieldErr -> {
			listDetail.add(new Detail(fieldErr.getField(), fieldErr.getDefaultMessage()));
		});

		return ErrorBuilder.builder().status(ErrorCode.INVALID_FIELD.getHttpStatus()).errors(listDetail)
				.message(ex.getLocalizedMessage()).build();

	}

}
