package com.weather.exceptionManager.error;

public class FactoryError {

	private static final String packageFolder = "com.weather.exceptionManager.error.";
	public static final String invalidParameterError = packageFolder + "InvalidParameterError";

	public static final IError createError(String type) {

		Class errorType = null;
		IError interfaceError = null;
		try {
			errorType = Class.forName(type);
			interfaceError = (IError) errorType.newInstance();

		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {

		}
		return interfaceError;

	}

}
