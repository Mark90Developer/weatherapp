package com.weather.exceptionManager;

import com.weather.exceptionManager.model.ErrorCode;

public class ApiException extends RuntimeException {

	private final ErrorCode errorCode;

	public ApiException(ErrorCode errorCode, String message) {
		super(message);
		this.errorCode = errorCode;
	}

}
