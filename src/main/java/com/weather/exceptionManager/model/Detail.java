package com.weather.exceptionManager.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Detail {
	private String field;
	private String reason;

}
