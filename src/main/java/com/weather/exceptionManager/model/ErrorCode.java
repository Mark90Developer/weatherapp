package com.weather.exceptionManager.model;

import org.springframework.http.HttpStatus;

import lombok.Getter;

@Getter
public enum ErrorCode {

	MISSING_FIELD(HttpStatus.BAD_REQUEST, "There is one or more fields that are missing"),
	INVALID_FIELD(HttpStatus.BAD_REQUEST, "There is one or more field are invalid");

	private HttpStatus httpStatus;
	private String message;

	ErrorCode(HttpStatus httpStatus, String message) {
		this.httpStatus = httpStatus;
		this.message = message;
	}

}
