package com.weather.exceptionManager.model;

import java.util.List;

import org.springframework.http.HttpStatus;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class ErrorBuilder {

	private HttpStatus status;
	private String message;
	private List<Detail> errors;

	public ErrorBuilder(HttpStatus status, String message, List<Detail> errors) {
		super();
		this.status = status;
		this.message = message;
		this.errors = errors;
	}

}