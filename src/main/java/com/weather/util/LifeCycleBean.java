package com.weather.util;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;

public class LifeCycleBean implements InitializingBean, DisposableBean {

	private static final Logger logging = LoggerFactory.getLogger(LifeCycleBean.class);

	@PostConstruct
	public void postConsructor() throws Exception {
		logging.info("1-postConsructor");
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		logging.info("2-afterPropertiesSet");

	}

	// used in configuration Bean
	public void init() throws Exception {
		logging.info("3-init");

	}

	@EventListener
	public void onApplicationEvent(ContextRefreshedEvent event) {
		logging.info("3-@EventListener");
	}

	@PreDestroy
	public void preDestroy() {
		logging.info("4-preDestroy");
	}

	@Override
	public void destroy() throws Exception {
		logging.info("5-destroy");

	}

	// used in configuration Bean
	public void afterDestroy() throws Exception {
		logging.info("6-afterDestroy");

	}

}
