package com.weather.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class CommonFunctionForAllBean implements BeanPostProcessor {

	private static final Logger logging = LoggerFactory.getLogger(CommonFunctionForAllBean.class);

	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {

		if (bean.getClass().getSimpleName().equalsIgnoreCase("WeatherController"))
			logging.info("1-postProcessBeforeInitialization" + bean);
		return bean;
	}

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		if (bean.getClass().getSimpleName().equalsIgnoreCase("WeatherController"))
			logging.info("2-postProcessAfterInitialization" + bean);
		return bean;
	}

}
