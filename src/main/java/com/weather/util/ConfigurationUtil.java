package com.weather.util;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfigurationUtil {

	@Bean(initMethod = "init", destroyMethod = "afterDestroy")
	public LifeCycleBean lifeCycleBean() {
		return new LifeCycleBean();
	}

}
