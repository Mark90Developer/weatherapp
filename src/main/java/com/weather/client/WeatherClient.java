package com.weather.client;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.weather.client.response.PollutionResponse;
import com.weather.client.response.WeatherResponse;
import com.weather.client.response.common.Coord;
import com.weather.client.response.common.Hourly;

@Component
public class WeatherClient {

	@Autowired
	private RestTemplate restTemplate;

	WeatherClient(@Value("${baseUrlWeatherClientRest}") String baseUrl, @Value("${appId}") String appId) {
		this.APP_ID = appId;
		this.urlWetherByCity = baseUrl + "weather?q={city}&APPID={appId}";
		this.urlDailyPollution = baseUrl + "air_pollution?lat={lat}&lon={lon}&appid={appId}";
		this.urlWeatherFor48HoursByCoordinates = baseUrl + "onecall?lat={latitude}&lon={longitude}&appid={appId}";
	}

	private final String APP_ID;
	private final String urlWetherByCity;
	private final String urlDailyPollution;
	private final String urlWeatherFor48HoursByCoordinates;

	// Are returned only the coordinate for the scope
	public Coord getWeatherAndCoordinatesByCity(String city) {
		ResponseEntity<WeatherResponse> rootResponseByCity = restTemplate.getForEntity(urlWetherByCity,
				WeatherResponse.class, city, APP_ID);
		WeatherResponse rootByCity = rootResponseByCity.getBody();
		return rootByCity.getCoord();
	}

	public List<Hourly> getWeatherFor24HoursByCoordinate(Coord coord) {

		ResponseEntity<WeatherResponse> rootResponseByCoordinates = restTemplate.getForEntity(
				urlWeatherFor48HoursByCoordinates, WeatherResponse.class, coord.getLat(), coord.getLon(), APP_ID);
		WeatherResponse rootByCoordinates = rootResponseByCoordinates.getBody();
		return rootByCoordinates.getHourly();

	}

	public PollutionResponse getDailyPollution(Coord coord) {
		ResponseEntity<PollutionResponse> pollutionResponseByCoordinates = restTemplate.getForEntity(urlDailyPollution,
				PollutionResponse.class, coord.getLat(), coord.getLon(), APP_ID);
		PollutionResponse pollutionResponse = pollutionResponseByCoordinates.getBody();
		return pollutionResponse;
	}

}
