package com.weather.client.response;

import com.weather.client.response.common.Coord;
import com.weather.client.response.common.List;

import lombok.Data;

@Data
public class PollutionResponse {
	private Coord coord;
	private java.util.List<List> list;
}
