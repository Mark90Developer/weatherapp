package com.weather.client.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.weather.client.response.common.Clouds;
import com.weather.client.response.common.Coord;
import com.weather.client.response.common.Current;
import com.weather.client.response.common.Daily;
import com.weather.client.response.common.Hourly;
import com.weather.client.response.common.Main;
import com.weather.client.response.common.Minutely;
import com.weather.client.response.common.Sys;
import com.weather.client.response.common.Weather;
import com.weather.client.response.common.Wind;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class WeatherResponse {
	private Coord coord;
	private List<Weather> weather;
	private String base;
	private Main main;
	private int visibility;
	private Wind wind;
	private Clouds clouds;
	private int dt;
	private Sys sys;
	private int id;
	private String name;
	private int cod;
	private double lat;
	private double lon;
	private String timezone;
	private int timezone_offset;
	private Current current;
	private List<Minutely> minutely;
	private List<Hourly> hourly;
	private List<Daily> daily;
}
