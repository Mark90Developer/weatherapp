package com.weather.client.response.common;

import java.util.List;

import lombok.Data;

@Data
public class Hourly{
	private int dt;
	private double temp;
	private double feels_like;
	private int pressure;
	private int humidity;
	private double dew_point;
    private double uvi;
    private int clouds;
    private int visibility;
    private double wind_speed;
    private int wind_deg;
    private double wind_gust;
    private List<Weather> weather;
    private int pop;
}
