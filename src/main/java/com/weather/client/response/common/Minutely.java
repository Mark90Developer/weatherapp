package com.weather.client.response.common;

import lombok.Data;

@Data
public class Minutely{
	private int dt;
	private int precipitation;
}