package com.weather.client.response.common;

import lombok.Data;

@Data
public class Components {
	private Double co;
	private Double no;
	private Double no2;
	private Double o3;
	private Double so2;
	private Double pm25;
	private Double pm10;
	private Double nh3;
}