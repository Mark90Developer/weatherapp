package com.weather.client.response.common;

import com.weather.client.response.pollution.Main;

import lombok.Data;

@Data
public class List {

	private Integer dt;
	private Main main;
	private Components components;
}