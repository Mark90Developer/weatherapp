package com.weather.client.response.common;

import lombok.Data;

@Data
public class Temp{
	private double day;
	private double min;
    private double max;
    private double night;
    private double eve;
    private double morn;
}