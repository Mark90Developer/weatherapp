package com.weather.dao.repository;

import org.springframework.data.repository.CrudRepository;

import com.weather.dao.entity.PollutionDaily;

public interface PollutionRepository extends CrudRepository<PollutionDaily, Long> {

}
