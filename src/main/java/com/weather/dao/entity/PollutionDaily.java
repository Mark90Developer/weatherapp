package com.weather.dao.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@Entity
@Table(name = "pollutiondaily", uniqueConstraints = { @UniqueConstraint(columnNames = { "dt", "CityName" }) })
public class PollutionDaily {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@NotNull
	private Integer dt;
	@NotNull
	@NotEmpty
	private String CityName;
	private Double co;
	private Double no;
	private Double no2;
	private Double o3;
	private Double so2;
	private Double pm25;
	private Double pm10;
	private Double nh3;

}
