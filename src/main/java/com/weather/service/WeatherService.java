package com.weather.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.weather.client.WeatherClient;
import com.weather.client.response.common.Coord;
import com.weather.client.response.common.Hourly;
import com.weather.controller.weather.WeatherFourtyEightHoursResponse;
import com.weather.model.weather.Hour;

@Service
public class WeatherService {

	@Autowired
	private WeatherClient weatherClient;

	public WeatherFourtyEightHoursResponse retrieveWeatherByCityFor48hours(String city) {
		Coord coord = weatherClient.getWeatherAndCoordinatesByCity(city);
		List<Hourly> hourlies = weatherClient.getWeatherFor24HoursByCoordinate(coord);
		List<Hour> hours = new ArrayList<Hour>();
		for (Hourly hourly : hourlies) {
			Hour hour = new Hour();
			hour.setHumidity(hourly.getHumidity());
			hour.setFeels_like(hourly.getFeels_like());
			hour.setTemp((hourly.getTemp()));
			hour.setDt(hourly.getDt());
			hours.add(hour);
		}
		WeatherFourtyEightHoursResponse response = new WeatherFourtyEightHoursResponse();
		response.setCityName(city);
		response.setHours(hours);
		return response;

	}

}
