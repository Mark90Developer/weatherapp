package com.weather.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.weather.client.WeatherClient;
import com.weather.client.response.PollutionResponse;
import com.weather.client.response.common.Coord;
import com.weather.controller.pollution.PollutionDailyRequest;
import com.weather.controller.pollution.PollutionDailyResponse;
import com.weather.converter.PollutionDailyConverter;
import com.weather.dao.entity.PollutionDaily;
import com.weather.dao.repository.PollutionRepository;
import com.weather.model.pollution.Components;

@Service
public class PollutionService {

	@Autowired
	private WeatherClient weatherClient;

	@Autowired
	PollutionDailyConverter pollutionDailyConverter;

	@Autowired
	PollutionRepository pollutionDailyRepository;

	public PollutionDailyResponse retrieveDailyPollutionByCity(String city) {

		Coord coord = weatherClient.getWeatherAndCoordinatesByCity(city);
		PollutionResponse pollutionResponse = weatherClient.getDailyPollution(coord);

		Components components = new Components();
		pollutionResponse.getList().get(0).getComponents();
		components.setCo(pollutionResponse.getList().get(0).getComponents().getCo());
		components.setNh3(pollutionResponse.getList().get(0).getComponents().getNh3());
		components.setNo(pollutionResponse.getList().get(0).getComponents().getNo());
		components.setO3(pollutionResponse.getList().get(0).getComponents().getO3());
		components.setPm10(pollutionResponse.getList().get(0).getComponents().getPm10());
		components.setPm25(pollutionResponse.getList().get(0).getComponents().getPm25());
		components.setSo2(pollutionResponse.getList().get(0).getComponents().getSo2());
		PollutionDailyResponse pollutionTodayResponse = new PollutionDailyResponse();
		pollutionTodayResponse.setCityName(city);
		pollutionTodayResponse.setComponents(components);
		pollutionTodayResponse.setDt(pollutionResponse.getList().get(0).getDt());
		return pollutionTodayResponse;

	}

	public void storeDailyPollutionByCity(PollutionDailyRequest pollutionDailyRequest) {
		PollutionDaily pollutionDaily = pollutionDailyConverter.convertFromDto(pollutionDailyRequest);
		pollutionDailyRepository.save(pollutionDaily);

	}

}
